import { Request } from "express";
import { makeHttpError } from "../helpers/http-error";
import { User } from "../db/user-model";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { Token } from "../db/token-model";
import { RefreshToken } from "../db/refreshtoken-model";

export function makeLoginEndpointHandler(httpRequest: Request) {
  switch (httpRequest.method) {
    case "POST":
      try {
        if (httpRequest.body.pass !== undefined) {
          return loginPassEmail(httpRequest);
        }
        return loginToken(httpRequest);
      } catch (err) {
        console.log(err);
      }
    default:
      return makeHttpError(405, `${httpRequest.method} method not allowed.`);
  }
}

async function loginPassEmail(httpRequest: Request) {
  const { pass, email } = httpRequest.body;
  const emailExist = await User.findOne({ email: email });
  if (emailExist === null) {
    return makeHttpError(404, "Invalid email");
  }
  const user = emailExist.toJSON();
  if (user.enabled === false) {
    return makeHttpError(400, "User disabled");
  }
  if ((await checkPass(pass, user.password)) === false) {
    return makeHttpError(400, "Invalid password");
  }

  const userData = {
    id: user.id,
    firstname: user.firstname,
    lastname: user.lastname,
    role: user.role,
  };
  const token = await jwt.sign({ user: userData }, process.env.TOKEN_KEY!, {
    expiresIn: "1h",
  });
  const refreshToken = await jwt.sign(
    { id: user.id },
    process.env.REFRESH_KEY!,
    { expiresIn: "1w" }
  );

  const tokenSave = new Token({
    id: user.id,
    token: token,
  });
  const refreshTokenSave = new RefreshToken({
    id: user.id,
    refreshToken: refreshToken,
  });
  await tokenSave.save();
  await refreshTokenSave.save();

  return {
    headers: {
      "Content-Type": "application/json",
    },
    statusCode: 200,
    data: JSON.stringify({ token: token, refreshToken: refreshToken }),
  };
}

async function checkPass(loginPass: string, registerPass: string) {
  const same = await bcrypt.compareSync(loginPass, registerPass);
  return same;
}

async function loginToken(httpRequest: Request) {
  const { refreshToken } = httpRequest.body;
  const refreshTokenValid = await RefreshToken.findOne({
    refreshToken: refreshToken,
  });
  if (refreshTokenValid === null) {
    console.log("itt");
    return makeHttpError(400, "Refresh token not valid!");
  }
  try {
    const decoded = jwt.verify(refreshToken, process.env.REFRESH_KEY!);
    const id = (decoded as any).id;
    await Token.findOneAndDelete({ id: id });
    await RefreshToken.findOneAndDelete({ id: id });

    const userDoc = await User.findOne({ id: id });
    if (userDoc === null) {
      return makeHttpError(404, "Invalid token");
    }
    const user = userDoc.toJSON();

    const userData = {
      id: user.id,
      firstname: user.firstname,
      lastname: user.lastname,
      role: user.role,
    };
    const newtoken = jwt.sign({ user: userData }, process.env.TOKEN_KEY!, {
      expiresIn: "1h",
    });
    const newRefreshToken = await jwt.sign(
      { id: user.id },
      process.env.REFRESH_KEY!,
      { expiresIn: "1w" }
    );
    const tokenSave = new Token({
      id: user.id,
      token: newtoken,
    });
    const refreshTokenSave = new RefreshToken({
      id: user.id,
      refreshToken: newRefreshToken,
    });

    await tokenSave.save();
    await refreshTokenSave.save();

    return {
      headers: {
        "Content-Type": "application/json",
      },
      statusCode: 200,
      data: JSON.stringify({ token: newtoken, refreshToken: newRefreshToken }),
    };
  } catch (err) {
    return makeHttpError(400, "Refresh token not valid!");
  }
}
