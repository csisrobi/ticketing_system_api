import { Request } from "express";
import { makeHttpError } from "../helpers/http-error";
import jwt from "jsonwebtoken";
import { Token } from "../db/token-model";
import { RefreshToken } from "../db/refreshtoken-model";

export function makeLogoutEndpointHandler(httpRequest: Request) {
  switch (httpRequest.method) {
    case "DELETE":
      return logoutToken(httpRequest);
    default:
      return makeHttpError(405, `${httpRequest.method} method not allowed.`);
  }
}

async function logoutToken(httpRequest: Request) {
  const { refreshToken } = httpRequest.body;
  const refreshTokenValid = await RefreshToken.findOne({
    refreshToken: refreshToken,
  });
  if (refreshTokenValid === null) {
    return makeHttpError(400, "Refresh token not valid!");
  }
  try {
    const decoded = jwt.verify(refreshToken, process.env.REFRESH_KEY!);
    const id = (decoded as any).id;
    await Token.findOneAndDelete({ id: id });
    await RefreshToken.findOneAndDelete({ id: id });
    return {
      headers: {
        "Content-Type": "application/json",
      },
      statusCode: 200,
      data: JSON.stringify("Logged out successfully"),
    };
  } catch (error) {
    return makeHttpError(400, "Refresh token not valid!");
  }
}
