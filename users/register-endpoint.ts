import { Request } from "express";
import { makeHttpError } from "../helpers/http-error";
import { User } from "../db/user-model";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { sendMail } from "./users-endpoint";
export function makeRegistrationEndpointHandler(httpRequest: Request) {
  switch (httpRequest.method) {
    case "POST":
      if (httpRequest.body.password !== undefined) {
        return changePassword(httpRequest);
      } else {
        return newToken(httpRequest);
      }
    default:
      return makeHttpError(405, `${httpRequest.method} method not allowed.`);
  }
}

async function changePassword(httpRequest: Request) {
  const userInfo = httpRequest.body;
  if (!userInfo) {
    return makeHttpError(400, "Bad request. No POST body.");
  }
  if (httpRequest.body.password !== httpRequest.body.passwordConfirm) {
    return {
      headers: {
        "Content-Type": "application/json",
      },
      statusCode: 400,
      data: JSON.stringify("Password does not match"),
    };
  }
  try {
    const decoded = jwt.verify(httpRequest.body.token, process.env.PASS_KEY!);
    const email = (decoded as any).email;
    const userWithEmail = await User.findOne({ email: email });
    if (userWithEmail === null) {
      return {
        headers: {
          "Content-Type": "application/json",
        },
        statusCode: 400,
        data: JSON.stringify("Token not valid"),
      };
    }
    const userWithTokenJson = userWithEmail.toJSON();
    const user = new User({
      _id: userWithTokenJson._id,
      id: userWithTokenJson.id,
      firstname: userWithTokenJson.firstname,
      lastname: userWithTokenJson.lastname,
      email: userWithTokenJson.email,
      role: userWithTokenJson.role,
      enabled: userWithTokenJson.enabled,
      password: await createPassword(httpRequest.body.password),
      passwordToken: "",
    });
    await User.findByIdAndUpdate(userWithTokenJson._id, user, {});
    return {
      headers: {
        "Content-Type": "application/json",
      },
      statusCode: 200,
      data: JSON.stringify("Password changed"),
    };
  } catch (e) {
    return {
      headers: {
        "Content-Type": "application/json",
      },
      statusCode: 400,
      data: JSON.stringify("Token not valid"),
    };
  }
}

async function createPassword(pass: string) {
  const saltRounds = 10;
  const password = pass.toString();
  const hashWithSalt = await bcrypt.hashSync(password, saltRounds);
  return hashWithSalt;
}

async function newToken(httpRequest: Request) {
  const { email } = httpRequest.body;
  if (!email) {
    return makeHttpError(400, "Bad request. No POST body.");
  }
  const token = await jwt.sign({ email: email }, process.env.PASS_KEY!, {
    expiresIn: "2h",
  });
  sendMail(httpRequest.body.email, token);
  const userWithEmail = await User.findOne({ email: email });
  const userWithTokenJson = userWithEmail!.toJSON();
  const user = new User({
    _id: userWithTokenJson._id,
    id: userWithTokenJson.id,
    firstname: userWithTokenJson.firstname,
    lastname: userWithTokenJson.lastname,
    email: userWithTokenJson.email,
    role: userWithTokenJson.role,
    enabled: userWithTokenJson.enabled,
    password: "",
    passwordToken: token,
  });
  await User.findByIdAndUpdate(userWithTokenJson._id, user, {});
  return {
    headers: {
      "Content-Type": "application/json",
    },
    statusCode: 200,
    data: JSON.stringify("New token sent"),
  };
}
