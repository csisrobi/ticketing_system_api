import { Request } from "express";
import { makeHttpError } from "../helpers/http-error";
import { User } from "../db/user-model";
import { v4 } from "uuid";
import bcrypt from "bcrypt";
import { sendMail } from "./users-endpoint";
import jwt from "jsonwebtoken";
export function makeAdminEndpointHandler(httpRequest: Request) {
  switch (httpRequest.method) {
    case "GET":
      return adminRole(httpRequest);
    default:
      return makeHttpError(405, `${httpRequest.method} method not allowed.`);
  }
}

async function adminRole(httpRequest: Request) {
  const adminExist = await User.findOne({ role: "admin" });
  if (adminExist !== null) {
    return {
      headers: {
        "Content-Type": "application/json",
      },
      statusCode: 300,
      data: JSON.stringify("Admin already exist"),
    };
  }
  const userInfo = httpRequest.body;
  if (!userInfo) {
    return makeHttpError(400, "Bad request. No POST body.");
  }
  const token = await jwt.sign({ email: process.env.email }, process.env.PASS_KEY!, {
    expiresIn: "2h",
  });
  const user = new User({
    id: v4(),
    firstname: "Csis",
    lastname: "Robert",
    email: process.env.email,
    role: "admin",
    enabled: true,
    password: '',
    passwordToken:token
  });
  sendMail(process.env.email!,token);
  user.save();
  return {
    headers: {
      "Content-Type": "application/json",
    },
    statusCode: 200,
    data: JSON.stringify("Successfully added!"),
  };
}
