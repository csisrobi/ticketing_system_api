import { Request } from "express";
import { makeHttpError } from "../helpers/http-error";
import { User } from "../db/user-model";
import { v4 } from "uuid";
import sendgrid from "@sendgrid/mail";
import jwt from "jsonwebtoken";
export function makeUsersEnpointHandler(httpRequest: Request) {
  switch (httpRequest.method) {
    case "GET":
      return getUsers();
    case "POST":
      return addUsers(httpRequest);
    case "PATCH":
      return editUsers(httpRequest);
    default:
      return makeHttpError(405, `${httpRequest.method} method not allowed.`);
  }
}

async function getUsers() {
  const result = await User.find();
  return {
    headers: {
      "Content-Type": "application/json",
    },
    statusCode: 200,
    data: JSON.stringify(result),
  };
}

async function addUsers(httpRequest: Request) {
  const userInfo = httpRequest.body;
  if (!userInfo) {
    return makeHttpError(400, "Bad request. No POST body.");
  }
  const token = await jwt.sign(
    { email: userInfo.email },
    process.env.PASS_KEY!,
    { expiresIn: "2h" }
  );
  sendMail(httpRequest.body.email, token);
  const user = new User({
    id: v4(),
    firstname: httpRequest.body.firstname,
    lastname: httpRequest.body.lastname,
    email: httpRequest.body.email,
    role: httpRequest.body.role,
    enabled: httpRequest.body.enabled,
    password: "",
    passwordToken: token,
  });

  user.save();
  return {
    headers: {
      "Content-Type": "application/json",
    },
    statusCode: 200,
    data: JSON.stringify("Successfully added!"),
  };
}

export function sendMail(email: string, token: string) {
  const apikey = process.env.API_KEY!;
  sendgrid.setApiKey(apikey);
  const msg = {
    to: email,
    from: "robicoc59@gmail.com",
    subject: "Password reset",
    text: process.env.CLIENT_HOST + "/register/" + token,
    html:
      "<p>Click <a href=" +
      process.env.CLIENT_HOST +
      "/register/" +
      token +
      ">here</a> to reset your password</p>",
  };
  sendgrid
    .send(msg)
    .then(() => {
      console.log("Message sent");
    })
    .catch((error) => {
      console.log(error.response.body);
    });
}

async function editUsers(httpRequest: Request) {
  const { firstname, lastname, email, role, enabled } = httpRequest.body;
  const id = httpRequest.params.id;
  const user = await User.findOne({ id: id });
  if (!user) {
    return makeHttpError(400, "The user is not existing!");
  }
  const userJSON = user.toJSON();
  const newUser = new User({
    _id: user._id,
    id: user.id,
    firstname: firstname === undefined ? userJSON.firstname : firstname,
    lastname: lastname === undefined ? userJSON.lastname : lastname,
    email: email === undefined ? userJSON.email : email,
    role: role === undefined ? userJSON.role : role,
    enabled: enabled === undefined ? userJSON.enabled : enabled,
  });
  await User.findByIdAndUpdate(userJSON._id, newUser);
  return {
    headers: {
      "Content-Type": "application/json",
    },
    statusCode: 200,
    data: JSON.stringify("Successfully updated!"),
  };
}
