import { Request } from 'express';
import { makeHttpError } from '../helpers/http-error';
import jwt from 'jsonwebtoken';

export function makeProfileEndpointHandler(httpRequest: Request) {
  switch (httpRequest.method) {
    case 'GET':
      return getUserProfile(httpRequest);
    default:
      return makeHttpError(405, `${httpRequest.method} method not allowed.`)
  }
}

async function getUserProfile(httpRequest: Request) {
  const header = httpRequest.headers['authorization'];
  const token = header!.split(' ')[1];
  const decoded = jwt.verify(token, process.env.TOKEN_KEY!);
  if (decoded === null) {
    return makeHttpError(400, 'Acces token denied');
  }
  const user = (decoded as any).user;
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    statusCode: 200,
    data: JSON.stringify({ firstname: user.firstname, lastname: user.lastname, role: user.role })
  }
}
