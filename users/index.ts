import { makeUsersEnpointHandler } from "./users-endpoint";
import { makeRegistrationEndpointHandler } from "./register-endpoint";
import { makeLoginEndpointHandler } from "./login-endpoint";
import { makeProfileEndpointHandler } from "./profile-endpoint";
import { makeLogoutEndpointHandler } from "./logout-endpoint";
import { makeAdminEndpointHandler } from "./admin-endpoint";
import { Request } from "express";

export function handleUserRequest(httpRequest: Request) {
  return makeUsersEnpointHandler(httpRequest);
}

export function handleRegistrationRequest(httpRequest: Request) {
  return makeRegistrationEndpointHandler(httpRequest);
}

export function handleLoginRequest(httpRequest: Request) {
  return makeLoginEndpointHandler(httpRequest);
}

export function handleProfileRequest(httpRequest: Request) {
  return makeProfileEndpointHandler(httpRequest);
}

export function handleLogoutRequest(httpRequest: Request) {
  return makeLogoutEndpointHandler(httpRequest);
}

export function handleAdminRequest(httpRequest: Request) {
  return makeAdminEndpointHandler(httpRequest);
}
