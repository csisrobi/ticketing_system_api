import db from 'mongoose';
const Schema = db.Schema;

const ticketSchema = new Schema({
  id: String,
  priority: String,
  title: String,
  description: String,
  assigned: String,
  date: String
});

export const Ticket = db.model('ticket', ticketSchema);
