import db from 'mongoose';
const Schema = db.Schema;

const userSchema = new Schema({
  id: String,
  firstname: String,
  lastname: String,
  email: String,
  role: String,
  enabled: Boolean,
  password: String,
  passwordToken:String
});

export const User = db.model('user', userSchema);
