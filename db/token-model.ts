import db from 'mongoose';
const Schema = db.Schema;

const tokenSchema = new Schema({
  id: String,
  token: String
});

export const Token = db.model('tokens', tokenSchema);
