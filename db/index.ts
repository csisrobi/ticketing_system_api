import db from 'mongoose';
const Schema = db.Schema;

export default async function connectDb() {
  db.connect(
    'mongodb://mongo:27017/todo',
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    }
  )
    .then(() => console.log('MongoDB Connected'))
    .catch((err: Error) => console.log(err));
}
