import db from 'mongoose';
const Schema = db.Schema;

const refreshTokenSchema = new Schema({
  id: String,
  refreshToken: String
});

export const RefreshToken = db.model('refreshtokens', refreshTokenSchema);
