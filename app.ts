import express, { NextFunction } from "express";
import logger from "morgan";
import bodyParser from "body-parser";
import cors from "cors";
import connectDb from "./db";
import { Request, Response } from "express";
import {
  handleUserRequest,
  handleRegistrationRequest,
  handleLoginRequest,
  handleProfileRequest,
  handleLogoutRequest,
  handleAdminRequest,
} from "./users";
import { handleTicketRequest } from "./tickets";
import jwt from "jsonwebtoken";
import { makeHttpError } from "./helpers/http-error";

const { PORT = 9002 } = process.env;

const app = express();

require("dotenv").config();
app.use(bodyParser.json());
app.use(cors());
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
connectDb();

app.all("/users", authAdminJWT, UserController);
app.all("/users/:id", authAdminJWT, UserController);
app.all("/tickets", authJWT, TicketController);
app.all("/tickets/:id", authJWT, TicketController);
app.post("/registration", RegistrationController);
app.post("/login", LoginController);
app.get("/profile", authJWT, ProfileController);
app.delete("/logout", authJWT, LogoutController);
app.get("/admin", AdminController);

function UserController(req: Request, res: Response) {
  handleUserRequest(req)
    .then(({ headers, statusCode, data }) =>
      res.set(headers).status(statusCode).send(data)
    )
    .catch((e: Error) => res.status(500).end());
}

function TicketController(req: Request, res: Response) {
  handleTicketRequest(req)
    .then(({ headers, statusCode, data }) =>
      res.set(headers).status(statusCode).send(data)
    )
    .catch((e: Error) => res.status(500).end());
}

function RegistrationController(req: Request, res: Response) {
  handleRegistrationRequest(req)
    .then(({ headers, statusCode, data }) =>
      res.set(headers).status(statusCode).send(data)
    )
    .catch((e: Error) => res.status(500).end());
}

function LoginController(req: Request, res: Response) {
  handleLoginRequest(req)
    .then(({ headers, statusCode, data }) =>
      res.set(headers).status(statusCode).send(data)
    )
    .catch((e: Error) => res.status(500).end());
}

function ProfileController(req: Request, res: Response) {
  handleProfileRequest(req)
    .then(({ headers, statusCode, data }) =>
      res.set(headers).status(statusCode).send(data)
    )
    .catch((e: Error) => res.status(500).end());
}

function LogoutController(req: Request, res: Response) {
  handleLogoutRequest(req)
    .then(({ headers, statusCode, data }) =>
      res.set(headers).status(statusCode).send(data)
    )
    .catch((e: Error) => res.status(500).end());
}

function AdminController(req: Request, res: Response) {
  handleAdminRequest(req)
    .then(({ headers, statusCode, data }) =>
      res.set(headers).status(statusCode).send(data)
    )
    .catch((e: Error) => res.status(500).end());
}

function authJWT(req: Request, res: Response, next: NextFunction) {
  const header = req.headers["authorization"];
  const token = header!.split(" ")[1];
  if (token !== undefined) {
    const decoded = jwt.verify(token, process.env.TOKEN_KEY!);
    if (decoded === null) {
      return makeHttpError(400, "Acces token denied");
    }
    next();
  } else {
    res
      .set({ "Content-Type": "application/json" })
      .status(403)
      .send(JSON.stringify("Acces denied"));
  }
}

function authAdminJWT(req: Request, res: Response, next: NextFunction) {
  const header = req.headers["authorization"];
  const token = header!.split(" ")[1];
  if (token !== undefined) {
    const decoded = jwt.verify(token, process.env.TOKEN_KEY!);
    if (decoded === null) {
      return makeHttpError(400, "Acces token denied");
    }
    const user = (decoded as any).user;
    if (user.role === "admin") {
      next();
    } else {
      res
        .set({ "Content-Type": "application/json" })
        .status(403)
        .send(JSON.stringify("Acces denied"));
    }
  } else {
    res
      .set({ "Content-Type": "application/json" })
      .status(403)
      .send(JSON.stringify("Acces denied"));
  }
}

app.listen(PORT, () => {
  console.log("server started at http://localhost:" + PORT);
});
