const path = require('path');
const {NODE_ENV = 'production',} = process.env;
const nodeExternals = require('webpack-node-externals');
const WebpackShellPlugin = require('webpack-shell-plugin');
  module.exports = {
    entry: './app.ts',
    mode: NODE_ENV,
    target: 'node',
    output: {
      path: path.resolve(__dirname, 'build'),
      filename: 'index.js'
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: [
            'ts-loader',
          ]
        }
      ]
    },
    resolve: {
      extensions: ['.ts', '.js'],
    },
    externals: [ nodeExternals() ],
    watch: true,
    plugins: [
      new WebpackShellPlugin({
        onBuildEnd: ['npm run run:dev']
      })
    ],
    node: {
      __dirname:true,
      __filename:true,
    }
  }