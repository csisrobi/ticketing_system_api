export async function makeHttpError (statusCode : number, errorMessage : String) {
    return {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode,
      data: JSON.stringify({
        success: false,
        error: errorMessage
      })
    }
  }
