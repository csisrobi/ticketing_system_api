import { makeTicketsEnpointHandler } from './tickets-endpoint';
import { Request, Response, NextFunction } from 'express';

export function handleTicketRequest(httpRequest: Request) {
  return makeTicketsEnpointHandler(httpRequest);
}
