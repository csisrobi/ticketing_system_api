import { Request, Response, NextFunction } from 'express';
import { makeHttpError } from '../helpers/http-error';
import { Ticket } from '../db/ticket-model';
import { v4 } from 'uuid';

export function makeTicketsEnpointHandler(httpRequest: Request) {
  switch (httpRequest.method) {
    case 'GET':
      return getTickets(httpRequest);
    case 'POST':
      return addTickets(httpRequest);
    case 'DELETE':
      return deleteTicket(httpRequest);
    case 'PATCH':
      return editTicket(httpRequest);
    default:
      return makeHttpError(405, `${httpRequest.method} method not allowed.`)
  }
}

async function getTickets(httpRequest: Request) {
  const result = await Ticket.find();
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    statusCode: 200,
    data: JSON.stringify(result)
  }
}

async function addTickets(httpRequest: Request) {
  const ticketInfo = httpRequest.body;

  if (!ticketInfo) {
    return makeHttpError(400, 'Bad request. No POST body.')
  }
  const ticket = new Ticket(
    {
      id: v4(),
      priority: httpRequest.body.priority,
      title: httpRequest.body.title,
      description: httpRequest.body.description,
      assigned: 'Unassigned',
      date: new Date().toISOString()
    }
  )

  const result = ticket.save();
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    statusCode: 200,
    data: JSON.stringify('Successfully added!')
  }
}

async function deleteTicket(httpRequest: Request) {
  const id = httpRequest.params.id;
  const ticket = await Ticket.findOne({ id: id });
  if (!ticket) {
    return makeHttpError(400, 'The ticket is not existing!');
  }
  await Ticket.findByIdAndDelete(ticket._id);
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    statusCode: 200,
    data: JSON.stringify('Successfully deleted!')

  }
}

async function editTicket(httpRequest: Request) {
  const { assigned, title, description, priority } = httpRequest.body;
  const id = httpRequest.params.id;
  const ticket = await Ticket.findOne({ id: id });
  if (!ticket) {
    return makeHttpError(400, 'The ticket is not existing!');
  }
  const ticketJSON = ticket.toJSON();
  const newTicket = new Ticket({
    _id: ticket._id,
    id: ticket.id,
    priority: (priority === undefined ? ticketJSON.priority : priority),
    title: (title === undefined ? ticketJSON.title : title),
    description: (description === undefined ? ticketJSON.description : description),
    assigned: (assigned === undefined ? ticketJSON.assigned : assigned),
    date: ticketJSON.date
  })
  await Ticket.findByIdAndUpdate(ticketJSON._id, newTicket);

  return {
    headers: {
      'Content-Type': 'application/json'
    },
    statusCode: 200,
    data: JSON.stringify('Successfully updated!')
  }
}
